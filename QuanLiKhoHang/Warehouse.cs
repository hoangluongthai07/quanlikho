﻿using System;
using System.Collections.Generic;

class Warehouse
{
    public int WarehouseCode { get; set; }
    public string WarehouseName { get; set; }
    public string Address { get; set; }
    public double Area { get; set; }
    public List<Product> Products { get; set; }

    //Tạo constructor để lưu các sản phẩm
    public Warehouse()
    {
        Products = new List<Product>();
    }
    //Thêm một sản phẩm mới
    public void AddProduct(Product newProduct)
    {    
        // Kiểm tra xem có sản phẩm nào có mã sản phẩm giống với mã sản phẩm mới không
        if (Products.Any(p => p.ProductCode == newProduct.ProductCode))
        {
            Console.WriteLine("Lỗi: Mã sản phẩm đã tồn tại. Không thể thêm sản phẩm mới.");
        }
        else
        {
            // Nếu không có sản phẩm nào có mã giống, thêm sản phẩm mới vào danh sách
            Products.Add(newProduct);
            Console.WriteLine("Thêm sản phẩm mới thành công.");
        }
    }
    //Xóa sản phẩm
    public void RemoveProduct(int productCode)
    {
        //Tìm sản phẩm cần xóa
        Product productRemove = Products.Find(p => p.ProductCode == productCode);
        //Kiểm tra xem sản phẩm có tồn tại hay không
        if (productRemove != null)
        {
            //Nếu tồn tại xóa khỏi ds
            Products.Remove(productRemove);
        }
    }
    //Cập nhật sản phẩm
    public void UpdateProduct(int productCode, Product updateProduct)
    {
        //Tìm sản phẩm cần update
        Product pToUpdate = Products.Find(p => p.ProductCode == productCode);
        //Kiểm tra xem sản phẩm có tồn tại không
        if (pToUpdate != null)
        {
            //Thực hiện cập nhật
            pToUpdate.ProductName = updateProduct.ProductName;
            pToUpdate.UnitPrice = updateProduct.UnitPrice;
            pToUpdate.ProductType = updateProduct.ProductType;
        }
    }
    //Xem sản phẩm
    public void DisplayProductList()
    {
        Console.WriteLine($"Product List in Warehouse {WarehouseName}:");

        foreach (Product product in Products)
        {
            Console.WriteLine($"Product Code: {product.ProductCode}, Product Name: {product.ProductName}, Quantity: {product.Quantity}, Unit Price: {product.UnitPrice}, Product Type: {product.ProductType}");
        }
    }

    //Search
    public Product SearchProduct(int productCode)
    {
        return Products.Find(p => p.ProductCode == productCode);
    }
}
