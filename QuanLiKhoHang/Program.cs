﻿using System;

class Program
{
    static void Main()
    {
        // Tạo một kho hàng
        Warehouse warehouse = new Warehouse
        {
            WarehouseCode = 1,
            WarehouseName = "Can Tho",
            Address = "123 Nguyen Van Cu",
            Area = 1000.0
        };

 
       
        Console.OutputEncoding = System.Text.Encoding.UTF8;
        while (true)
        {
            Console.WriteLine("\n===== Menu =====");
            Console.WriteLine("1. Hiển thị danh sách sản phẩm");
            Console.WriteLine("2. Thêm sản phẩm mới");
            Console.WriteLine("3. Cập nhật thông tin sản phẩm");
            Console.WriteLine("4. Xóa sản phẩm");
            Console.WriteLine("5. Tìm kiếm sản phẩm");
            Console.WriteLine("6. Thoát");
            Console.Write("Chọn một số: ");

            string choice = Console.ReadLine();

            switch (choice)
            {
                case "1":
                    // Hiển thị danh sách sản phẩm
                    warehouse.DisplayProductList();
                    break;
                case "2":
                    // Thêm sản phẩm mới
                    AddNewProduct(warehouse);
                    break;
                case "3":
                    // Cập nhật thông tin sản phẩm
                    UpdateProduct(warehouse);
                    break;
                case "4":
                    // Xóa sản phẩm
                    RemoveProduct(warehouse);
                    break;
                case "5":
                    // Tìm kiếm sản phẩm
                    SearchProduct(warehouse);
                    break;
                case "6":
                    // Thoát
                    return;
                default:
                    Console.WriteLine("Lựa chọn không hợp lệ. Hãy chọn lại.");
                    break;
            }
        }
    }

    static void AddNewProduct(Warehouse warehouse)
    {
        Console.Write("Nhập mã sản phẩm: ");
        if (!int.TryParse(Console.ReadLine(), out int productCode))
        {
            Console.WriteLine("Lỗi: Mã sản phẩm phải là một số nguyên.");
            return;
        }

        Console.Write("Nhập tên sản phẩm: ");
        string productName = Console.ReadLine();

        Console.Write("Nhập số lượng: ");
        if (!int.TryParse(Console.ReadLine(), out int quantity) || quantity < 0)
        {
            Console.WriteLine("Lỗi: Số lượng phải là một số nguyên không âm.");
            return;
        }

        Console.Write("Nhập đơn giá: ");
        if (!decimal.TryParse(Console.ReadLine(), out decimal unitPrice) || unitPrice < 0)
        {
            Console.WriteLine("Lỗi: Đơn giá phải là một số thực không âm.");
            return;
        }

        Console.Write("Nhập loại sản phẩm: ");
        string productType = Console.ReadLine();

        // Tạo đối tượng sản phẩm mới
        Product newProduct = new Product
        {
            ProductCode = productCode,
            ProductName = productName,
            Quantity = quantity,
            UnitPrice = unitPrice,
            ProductType = productType
        };

        // Thêm sản phẩm mới vào kho
        warehouse.AddProduct(newProduct);
        Console.WriteLine("Thêm sản phẩm mới thành công.");
    }


    static void UpdateProduct(Warehouse warehouse)
    {
        Console.Write("Nhập mã sản phẩm cần cập nhật: ");
        int productCode = int.Parse(Console.ReadLine());

        // Tìm sản phẩm theo mã sản phẩm
        Product existingProduct = warehouse.SearchProduct(productCode);

        if (existingProduct != null)
        {
            Console.Write("Nhập tên sản phẩm mới: ");
            string newName = Console.ReadLine();

            Console.Write("Nhập giá sản phẩm mới: ");
            decimal newPrice = decimal.Parse(Console.ReadLine());

            Console.Write("Nhập loại sản phẩm mới: ");
            string newType = Console.ReadLine();

            // Tạo đối tượng Product mới để cập nhật
            Product updatedProduct = new Product
            {
                ProductName = newName,
                UnitPrice = newPrice,
                ProductType = newType
            };

            // Cập nhật sản phẩm
            warehouse.UpdateProduct(productCode, updatedProduct);
            Console.WriteLine("Cập nhật thành công.");
        }
        else
        {
            Console.WriteLine("Không tìm thấy sản phẩm có mã " + productCode);
        }
    }

    static void RemoveProduct(Warehouse warehouse)
    {
        Console.Write("Nhập mã sản phẩm cần xóa: ");
        int productCode = int.Parse(Console.ReadLine());

        // Xóa sản phẩm
        warehouse.RemoveProduct(productCode);
        Console.WriteLine("Xóa sản phẩm thành công.");
    }

    static void SearchProduct(Warehouse warehouse)
    {
        Console.Write("Nhập mã sản phẩm cần tìm: ");
        int productCode = int.Parse(Console.ReadLine());

        // Tìm sản phẩm
        Product foundProduct = warehouse.SearchProduct(productCode);

        if (foundProduct != null)
        {
            Console.WriteLine($"Sản phẩm có mã {productCode}: {foundProduct.ProductName}, Quantity: {foundProduct.Quantity}, Unit Price: {foundProduct.UnitPrice}");
        }
        else
        {
            Console.WriteLine("Không tìm thấy sản phẩm có mã " + productCode);
        }
    }
}
